import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AnimacionesService } from './animaciones.service';

import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { LoginPage } from '../pages/login/login.page';

@Injectable({
  providedIn: 'root'
})
export class HelperGlobalService {

  constructor(
    private navCtrl: NavController,
    private aniServ: AnimacionesService,
    private nativePageTransition: NativePageTransitions
  ) { }

  openUri(uri: string){
    window.open(uri, "_system");
  }

  openPage(slug: string, dir: string, time:number ){
    this.aniServ.transitionWithSlide(dir, time);
    this.navCtrl.navigateRoot(slug || "/");
  }

  goPageWithAnimation(animation: string = "fade", slug: string = "index", direction: string = "up", timeout: number = 400)
  {
    let options: NativeTransitionOptions = {
      direction: direction,
      duration: timeout,
      slowdownfactor: 5,
      slidePixels: 40,
      iosdelay: 200,
      androiddelay: 300,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    };

    if( animation === "fade" ){
      this.nativePageTransition
      .fade(options)
      .then((done)=>{
        
      })
      .catch((error)=>{
      });

      this.navCtrl.navigateForward(slug);
    }

    if( animation === "draw" ){
      this.nativePageTransition
      .drawer(options)
      .then((done)=>{
        
      })
      .catch((error)=>{
      });

      this.navCtrl.navigateForward(slug);
    }
    
    if( animation === "slide" ){
      this.nativePageTransition
      .slide(options)
      .then((done)=>{
        
      })
      .catch((error)=>{
      });

      this.navCtrl.navigateForward(slug);
    }
    
    if( animation === "flip" ){
      this.nativePageTransition
      .flip(options)
      .then((done)=>{
        
      })
      .catch((error)=>{
      });

      this.navCtrl.navigateForward(slug);
    }
  }

  goTo(){

    let options: NativeTransitionOptions = {
      direction: 'up',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    };

    this.nativePageTransition
    .flip(options)
    .then( (done) => {
      console.log(done);
    } )
    .catch( (error) => {
      console.log(error);
    } );

    this.navCtrl.navigateForward('login');
  }
}
