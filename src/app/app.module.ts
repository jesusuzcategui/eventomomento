import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { HttpClientModule } from '@angular/common/http';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { IonicStorageModule } from '@ionic/storage-angular';

/** OWN MODULES*/
import { GalleryfromgoogleModule } from './galleryfromgoogle/galleryfromgoogle.module';
import { HelperGlobalService } from './services/helper-global.service';
import { AccesosService } from './services/accesos.service';
import { ApiRestService } from './services/api-rest.service';
import { AnimacionesService } from './services/animaciones.service';
import { NativePageTransitions } from '@ionic-native/native-page-transitions/ngx';
import { ThemeDetection, ThemeDetectionResponse } from '@ionic-native/theme-detection';
/** OWN MODULES*/

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    IonicStorageModule.forRoot(),
    HttpClientModule,

    /***************************/
    GalleryfromgoogleModule,
    /***************************/
  ],
  providers: [
    HelperGlobalService,
    AccesosService,
    ApiRestService,
    AnimacionesService,
    NativePageTransitions,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(){
    ThemeDetection
    .isAvailable()
    .then( (resp: ThemeDetectionResponse) => {
      if( resp.value ){
        ThemeDetection
        .isDarkModeEnabled()
        .then( (res: ThemeDetectionResponse) => {
          console.log(resp);
        })
        .catch( (err) => {
          console.error(err);
        } );
      }
    } )
    .catch( (error: any) => {
      console.error(error);
    } );
  }
}
