import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-portfolio-detailt',
  templateUrl: './portfolio-detailt.page.html',
  styleUrls: ['./portfolio-detailt.page.scss'],
})
export class PortfolioDetailtPage implements OnInit {

  @Input() image: string;
  @Input() title: string;
  @Input() intro: any;
  @Input() link: string;

  constructor(
    private modalCtrl: ModalController,
    private sanitaizer: DomSanitizer
  ) { }

  ngOnInit() {
    let Html = document.getElementById("introtext");
    let Intro : any = this.sanitaizer.bypassSecurityTrustHtml( this.intro );
        Intro = Intro.changingThisBreaksApplicationSecurity;

    Html.innerHTML = Intro;
  }

  closeDetails(){
    this.modalCtrl.dismiss();
  }

  openLink(url: string){
    window.open(url, "_system");
  }

}
